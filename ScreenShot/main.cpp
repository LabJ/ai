﻿#pragma once


//opencv
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/core/ocl.hpp" 


#include <Windows.h>
#include <wincodec.h>
#include <Winuser.h>

#include "time.h"
#include <math.h>

#include <iostream>
#include <cstdio>
#include <string.h>


using namespace std;
using namespace cv;

#define LEVEL 30
#define LEFT 37
#define RIGHT 39
#define JUMP 88

#pragma optimize("", on)

//게임 학습을 위한 변수들
int keyCounter = 0; //임시 카운터
int currentSpeed = 0; //플레이 결과에따른 속도
int playCount = 0; //플레이 횟수를 카운트한다
int endCount = 20500; //게임을 종료시킬 시점
int over = 0; //게임오버 횟수
int clear = 0; //클리어 횟수
int jumpStatus = 0; //점프상태

//시간 확인용 변수
clock_t start;

//입력된 키값에 대한 구조체
struct keyMemory {
	long long key = 1;
	int left = 0;
	int right = 0;
	int jump = 0;
	int input = 0;
};

//입력되었던 값 기록
struct keyMemory KM[LEVEL]; 

//아웃풋 전환을 위한 임시 버퍼
char fout[30] = "";
char lay[30] = "";

//캡쳐 처리를 위한 화면 위치
int minX = 12;
int minY = 89;
int maxX = 262;
int maxY = 289;

//////////////////////
////영상 처리 파트////
//////////////////////

//dib 선언
BITMAPINFO dib_define;
BITMAPFILEHEADER dib_format_layout;

// DIB의 내부 이미지 비트 패턴을 참조할 포인터 변수
BYTE *p_image_data = NULL;

// 스크린 전체를 캡쳐하기 위해서 CWindowDC 형식으로 DC를 얻는다.
// GetDC의 파라메터에 특정 윈도우 핸들을 넣지 않고 NULL을 넣으면
// CWindowDC 형식으로 DC를 얻게 된다.
HDC h_screen_dc = GetDC(NULL);

// 이미지를 추출하기 위해서 가상 DC를 생성한다. 메인 DC에서는 직접적으로 비트맵에 접근하여 이미지 패턴을 얻을 수 없기 때문이다.
HDC h_memory_dc = CreateCompatibleDC(h_screen_dc);

//템플릿 매칭을 위한 이미지들을 가져온다
Mat num0 = imread("pic/num0.bmp");
Mat gameover = imread("pic/gameover.bmp");
Mat spd = imread("pic/spd.bmp");
Mat spd9 = imread("pic/spd9.bmp");

Mat tmp_sp = imread("pic/sp.bmp");
Mat tmp_char = imread("pic/char.bmp");

Mat trap11 = imread("pic/trap1-1.bmp");
Mat trap12 = imread("pic/trap1-2.bmp");
Mat trap12m = imread("pic/trap1-2m.bmp");
Mat trap13 = imread("pic/trap1-3.bmp");
Mat trap14 = imread("pic/trap1-4.bmp");
Mat trap15 = imread("pic/trap1-5.bmp");
Mat trap15s = imread("pic/trap1-5s.bmp");

Mat trap21 = imread("pic/trap2-1.bmp");
Mat trap22 = imread("pic/trap2-2.bmp");
Mat trap23 = imread("pic/trap2-3.bmp");

//캡쳐한 화면 이미지를 위한 변수
Mat res;
Mat ref_img;
Mat ref_gray;
Mat roi;

//roi설정을 저장하기위한 변수
Point roiMin;
Point roiMax;

//캡쳐와 저장을 임시로 묶은 함수 ,  bitbit을 이용해 캡쳐한다
void capture(char fname[])
{
	// 현재 스크린 화면을 캡쳐한다.
	BitBlt(h_memory_dc, 0, 0, maxX, maxY, h_screen_dc, minX, minY, SRCCOPY);

	//파일로 저장
	FILE *p_file;
	fopen_s(&p_file, fname, "wb");
	if (p_file != NULL) {
		fwrite(&dib_format_layout, 1, sizeof(BITMAPFILEHEADER), p_file);
		fwrite(&dib_define, 1, sizeof(BITMAPINFOHEADER), p_file);
		fwrite(p_image_data, 1, dib_define.bmiHeader.biSizeImage, p_file);
		fclose(p_file);
	}

}

//roi를 설정하는 함수 좌표값을 가공하기위해 따로 함수로 구현
Mat setRoi(Mat img_org, int minX, int minY, int maxX, int maxY)
{
	roiMin = Point(minX, minY); //시작 x.y좌표 설정
	roiMax = Point(maxX, maxY); //종료 x.y좌표 설정

	//rectangle(ref_img, roiMin, roiMax, CV_RGB(0, 255, 0), 1); //roi 범위 시각화

	return img_org(Rect(roiMin, roiMax)); //설정된 roi 리턴
}

//tamplate 매칭을 이용해 찾은 위치에 사각형을 그려 시각화 해준다
void multiDraw(Mat ref_img, Mat res, Mat tpl_img, double thres)
{
	double minval, maxval;
	Point minloc, maxloc;

	threshold(res, res, 0.9, 1., CV_THRESH_TOZERO);

	while (true)
	{
		minMaxLoc(res, &minval, &maxval, &minloc, &maxloc);

		if (maxval >= thres)
		{
			//해당 좌표값을 출력으로 전환
			sprintf(fout, "%s%d", fout, maxloc.x / 5); 
			
			//maxloc = maxloc + roiMin;
			//rectangle(ref_img, maxloc,Point(maxloc.x + tpl_img.cols, maxloc.y + tpl_img.rows),CV_RGB(255, 0, 0), 1);
			//maxloc = maxloc - roiMin;

			//처리한 매칭점을 지운다
			floodFill(res, maxloc,	Scalar(0), 0,	Scalar(.1),	Scalar(1.) );
		}
		else
		{
			//모든 점이 처리되면 while문 종료
			break;
		}
	}
}

//tamplate 매칭을 이용해 찾은 위치에 사각형을 그려 시각화 해준다
void draw(Mat ref_img, Mat tpl_img, Point maxloc)
{
	//maxloc = maxloc + roiMin;
	//rectangle(ref_img, maxloc, Point(maxloc.x + tpl_img.cols, maxloc.y + tpl_img.rows), CV_RGB(255, 0, 0), 1);
}


//멀티 템플릿매칭
void multiMatch(Mat org_img,  //확인할 이미지
	Mat tpl_img,  // 매칭할 이미지
	Mat res,	// 결과 이미지
	Mat ref_img,   // 출력할 원본 이미지
	double thres) //threshhold 값
{
	//템플릿 매칭을 하고 위의 draw함수로 처리한다, 정확도를 위해 VC_TM_CCOEFF_NORMED 사용
	matchTemplate(org_img, tpl_img, res, CV_TM_CCOEFF_NORMED);
	multiDraw(ref_img, res, tpl_img, thres);
}



//작은 함정을 한번만 템플리 매칭하는 y축의 정보만 넘긴다
void simpleMatch(Mat org_img,  //확인할 이미지
	Mat tpl_img,  // 매칭할 이미지
	Mat res,	// 결과 이미지
	Mat ref_img,   // 출력할 원본 이미지
	double thres, //threshhold 값
	int type) //매칭을 비교하기위한 값
{
	double minval, maxval;
	Point minloc, maxloc;

	//속도를 위해 CV_TM_CCORR_NORMED를 사용한다
	matchTemplate(org_img, tpl_img, res, CV_TM_CCORR_NORMED);
	threshold(res, res, 0.9, 1., CV_THRESH_TOZERO);


	minMaxLoc(res, &minval, &maxval, &minloc, &maxloc);

	if (maxval >= thres)
	{
		//해당 좌표값을 출력으로 전환
		sprintf(fout, "%s%d%d", fout, type, maxloc.y/2); 
		draw(ref_img, tpl_img, maxloc);
	}
}

//긴 함정을 위한 매칭 x,y좌표를 모두 넘긴다
void longMatch(Mat org_img,  //확인할 이미지
	Mat tpl_img,  // 매칭할 이미지
	Mat res,	// 결과 이미지
	Mat ref_img,   // 출력할 원본 이미지
	double thres, //threshhold 값
	int type) //매칭을 비교하기위한 값
{
	double minval, maxval;
	Point minloc, maxloc;

	matchTemplate(org_img, tpl_img, res, CV_TM_CCORR_NORMED);
	threshold(res, res, 0.9, 1., CV_THRESH_TOZERO);


	minMaxLoc(res, &minval, &maxval, &minloc, &maxloc);

	if (maxval >= thres)
	{
		//해당 좌표값을 출력으로 전환
		sprintf(fout, "%s%d%d%d", fout, type, maxloc.x / 2, maxloc.y/2);
		draw(ref_img, tpl_img, maxloc);
	}
}

//팽귄을 템플릿 매칭하는 함수
void charMatch(Mat org_img,  //확인할 이미지
	Mat tpl_img,  // 매칭할 이미지
	Mat res,	// 결과 이미지
	Mat ref_img,   // 출력할 원본 이미지
	double thres) //threshhold 값
{
	double minval, maxval;
	Point minloc, maxloc;

	matchTemplate(org_img, tpl_img, res, CV_TM_CCORR_NORMED);
	threshold(res, res, 0.7, 1., CV_THRESH_TOZERO);


	minMaxLoc(res, &minval, &maxval, &minloc, &maxloc);

	if (maxval >= thres)
	{
		if (maxloc.y == 16) //점프를 하지 않은 경우 jumpstatus를 0으로 입력
		{
			//좌표를 출력으로 전환
			sprintf(fout, "%s%d%d", fout, 1, maxloc.x/2); 
			jumpStatus = 0;
		}
		else //아닐경우 1로 입력
		{
			//좌표를 출력으로 전환
			sprintf(fout, "%s%d%d", fout, 3, maxloc.x/2);
			jumpStatus = 1;
		}
		draw(ref_img, tpl_img, maxloc);
	}
	else //팽귄이 찾아지지 않는경우 9로 입력
	{
		sprintf(fout, "%s%d", fout, 2);
		jumpStatus = 9;
	}
}

/////////////////////
//// 인공지능파트////
/////////////////////

//키 입력을 초기화한다
void keyReset()
{
	keybd_event(VK_UP, 0, KEYEVENTF_KEYUP, 0);
	keybd_event(VK_UP, 0, 0, 0);
	keybd_event(VK_LEFT, 0, KEYEVENTF_KEYUP, 0);
	keybd_event(VK_RIGHT, 0, KEYEVENTF_KEYUP, 0);

	//	keybd_event(VK_DOWN, 0, KEYEVENTF_KEYUP, 0); 
	keybd_event(JUMP, 0, KEYEVENTF_KEYUP, 0);
}

//선택한 키를 입력한다
void keyInput(int keyNum)
{
	keybd_event(keyNum, 0, 0, 0);
}

//기존의 play횟수 기록을 파일에서 가져온다
void loadPlayCount()
{
	FILE* in = fopen("ai/PlayCount.txt", "r");
	if (in != NULL)
	{
		fscanf(in, "%d %d %d", &playCount, &clear, &over);
		fclose(in);
	}
	else 
	{
		FILE* out = fopen("ai/PlayCount.txt", "w");
		fprintf(out, "%d %d %d", playCount, clear, over);
		fclose(out);
	}
}

//종료지점에 도착하여 게임을 클리어 했을떄의 처리
void clearGame(Mat org_img,  //확인할 이미지
	Mat tpl_img) // 매칭할 이미지)
{
	double minval, maxval;
	Point minloc, maxloc;

	matchTemplate(org_img, tpl_img, res, CV_TM_CCORR_NORMED);
	threshold(res, res, 0.9, 1., CV_THRESH_TOZERO);

	minMaxLoc(res, &minval, &maxval, &minloc, &maxloc);

	//클리어 했으면
	if (maxval >= 1)
	{
		//게임을 초기화하고 플레이 기록을 업데이트한다
		keyReset();
		keybd_event(VK_F1, 0, 0, 0);
		Sleep(100);
		keybd_event(VK_F1, 0, KEYEVENTF_KEYUP, 0);
		playCount++;
		clear++;

		//플레이 결과 출력
		cout << playCount << "  time:" << clock() - start <<" clear"<< endl;

		//플레이 횟수를 파일에 쓰고
		FILE* out = fopen("ai/PlayCount.txt", "w");
		fprintf(out, "%d %d %d", playCount, clear, over);
		fclose(out);

		//플레이 결과도 파일에 쓴다
		FILE* pResult = fopen("ai/PlayResult.txt", "a");
		if (pResult == NULL)
			FILE* pResult = fopen("ai/PlayResult.txt", "w");
		fprintf(pResult, "%d %d clear\n", playCount, clock() - start);
		fclose(pResult);

		//입력했던 key값 초기화
		for(int i=0; i< LEVEL; i++)
			KM[i].key = 0;

		//시간값을 초기화하고 설정된 종료횟수에 도달하면 프로그램을 종료한다
		start = clock();
		if (playCount == endCount)
			exit(0);

	}
}

//game over에 대한 처리
void gameOver(Mat org_img,  //확인할 이미지
	Mat tpl_img) // 매칭할 이미지)
{
	double minval, maxval;
	Point minloc, maxloc;

	matchTemplate(org_img, tpl_img, res, CV_TM_CCORR_NORMED);
	threshold(res, res, 0.9, 1., CV_THRESH_TOZERO);

	minMaxLoc(res, &minval, &maxval, &minloc, &maxloc);

	//게임오버가 되면
	if (maxval >= 1)
	{
		//게임을 초기화하고 플레이 정보 update
		keybd_event(VK_F1, 0, 0, 0);
		Sleep(100);
		keybd_event(VK_F1, 0, KEYEVENTF_KEYUP, 0);
		playCount++;
		over++;

		//입력된 키값 초기화
		for (int i = 0; i< LEVEL; i++)
			KM[i].key = 0;

		//플레이 횟수를 파일에 기록
		cout << playCount << "  time:" << clock() - start << endl;
		FILE* out = fopen("ai/PlayCount.txt", "w");
		fprintf(out, "%d %d %d", playCount, clear, over);
		fclose(out);

		//플레이 결과를 파일에 기록
		FILE* pResult = fopen("ai/PlayResult.txt", "a");
		if (pResult == NULL)
			FILE* pResult = fopen("ai/PlayResult.txt", "w");
		fprintf(pResult, "%d %d \n", playCount, clock() - start);
		fclose(pResult);
		
		//시간값을 초기화하고 지정된 횟수에 도달하면 프로그램 종료
		start = clock();
		if (playCount == endCount)
		{
			exit(0);
		}
	}

}

//속도를 체크해서 결과 정보를 update한다
int checkSpd(Mat org_img,  //확인할 이미지
	Mat tpl_img)  // 매칭할 이미지
{
	double minval, maxval;
	Point minloc, maxloc;

	matchTemplate(org_img, tpl_img, res, CV_TM_CCORR_NORMED);
	threshold(res, res, 0.9, 1., CV_THRESH_TOZERO);

	minMaxLoc(res, &minval, &maxval, &minloc, &maxloc);

	
	if (maxval >= 1)
	{
		//최저속도의 경우
		currentSpeed = 0;
		return 0;

	}
	else
	{
		//최저속도가 아닐경우
		currentSpeed = 1;
		return 1;
	}
}

//속도를 체크해서 결과 정보를 update한다
void fullSpd(Mat org_img,  //확인할 이미지
	Mat tpl_img)  // 매칭할 이미지
{
	double minval, maxval;
	Point minloc, maxloc;

	matchTemplate(org_img, tpl_img, res, CV_TM_CCORR_NORMED);
	threshold(res, res, 0.9, 1., CV_THRESH_TOZERO);

	minMaxLoc(res, &minval, &maxval, &minloc, &maxloc);

	if (maxval >= 1)
	{
		//최고속도의 경우
		currentSpeed = 9;
	}
}

//좋은 플레이 결과에 대한 처리
void goodUpdate(int i)
{
	if (KM[i].input == LEFT)
	{
		KM[i].right = abs(KM[i].right - 12);
		KM[i].left += 14;
		KM[i].jump = abs(KM[i].jump - 2);
		if (KM[i].left > 1600)
		{
			KM[i].left = 1300;
			KM[i].right = 0;
			KM[i].jump = 0;
		}
	}
	else if (KM[i].input == RIGHT)
	{
		KM[i].right += 14;
		KM[i].left = abs(KM[i].left - 12);
		KM[i].jump = abs(KM[i].jump - 2);
		if (KM[i].right > 1600)
		{
			KM[i].right = 1300;
			KM[i].left = 0;
			KM[i].jump = 0;
		}
	}
	else if (KM[i].input == JUMP)
	{
		KM[i].right = abs(KM[i].right - 5);
		KM[i].left = abs(KM[i].left - 5);
		KM[i].jump += 10;
		if (KM[i].jump > 1600)
		{
			KM[i].jump = 1300;
			KM[i].left = 0;
			KM[i].right = 0;
		}
	}
}

//안좋은 플레이 결과에 대한 처리
void badUpdate()
{
	//입력된 기존 값에 대해 일괄적으로 패널티를 준다
	for (int i = 0; i < LEVEL; i++)
	{
		sprintf(lay, "%s%lld%s", "ai/", KM[i].key, ".txt");
		FILE* update = fopen(lay, "w");

		//각각 왼쪽,오른쪽 점프키 입력에 대한 패널티
		if (KM[i].input == LEFT)
		{
			KM[i].right += 240;
			KM[i].left -= 280;
			KM[i].jump += 40;
			if (KM[i].left < 0)
			{
				KM[i].left = 0;
			}
		}
		else if (KM[i].input == RIGHT)
		{
			KM[i].right -= 280;
			KM[i].left += 240;
			KM[i].jump += 40;
			if (KM[i].right < 0)
			{
				KM[i].right = 0;
			}
		}
		else if (KM[i].input == JUMP)
		{
			KM[i].right += 150;
			KM[i].left += 150;
			KM[i].jump -= 300;
			if (KM[i].jump < 0)
			{
				KM[i].jump = 0;
			}
		}

		//갱신된 정보를 파일에 업데이트
		fprintf(update, "%d %d %d", KM[i].left, KM[i].right, KM[i].jump);
		fclose(update);
		KM[i].key = 0;
	}

	//플레이 횟수를 파일에 쓴다
	playCount++;
	cout << playCount << "  time:" << clock() - start << endl;
	FILE* out = fopen("ai/PlayCount.txt", "w");
	fprintf(out, "%d %d %d", playCount, clear, over);
	fclose(out);

	//플레이 결과도 파일에 쓴다
	FILE* pResult = fopen("ai/PlayResult.txt", "a");
	if (pResult == NULL)
		FILE* pResult = fopen("ai/PlayResult.txt", "w");
	fprintf(pResult, "%d %d \n", playCount, clock() - start);
	fclose(pResult);

	//시간값 초기화
	start = clock();

	//게임 리셋
	keybd_event(VK_F1, 0, 0, 0);
	Sleep(100);
	keybd_event(VK_F1, 0, KEYEVENTF_KEYUP, 0);

	//지정한 플레이 횟수에 도달하면 프로그램 종료
	if (playCount == endCount)
		exit(0);
}

//속도 결과를 확인해서 입력데이터를 업데이트한다
void updateData()
{

	for (int i = 0; i < LEVEL; i++)
	{
		sprintf(lay, "%s%lld%s", "ai/", KM[i].key, ".txt");
		FILE* update = fopen(lay, "w");

		//최고속도일 경우 입력에 대한 긍정적인 처리
		if (currentSpeed == 9)
		{
			goodUpdate(i);
		} else if(currentSpeed == 0) //최저속도일경우 안좋은 결과
			badUpdate();
		
		//업데이트된 정보를 파일에 쓴다
		fprintf(update, "%d %d %d", KM[i].left, KM[i].right, KM[i].jump);
		fclose(update);
	}
}

//키보드 입력과 기록에 대한 처리
//캐릭터와 함정의 위치정보 key값으로 받는다
void layer(long long key)
{
	//기존 입력값 초기화
	keyReset();

	//점프중에는 키입력이 소용없기떄문에 점프중이 아닐때만 처리한다
	if (jumpStatus != 1)
	{
		//기존값에 대한 피드백을 위한 update 실행
		updateData();
		int keybd = 0;

		//기존의 key에 대한 처리를 가져온다
		sprintf(lay, "%s%lld%s", "ai/", key, ".txt");
		FILE* in = fopen(lay, "r");

		//key값 입력
		KM[keyCounter].key = key;
		
		//기존값이 없으면 초기화
		if (in == NULL)
		{
			KM[keyCounter].left = 600;
			KM[keyCounter].right = 600;
			KM[keyCounter].jump = 100;
		}
		else //존재하면 읽어온다
		{
			fscanf(in, "%d %d %d", &KM[keyCounter].left, &KM[keyCounter].right, &KM[keyCounter].jump);
		}

		//파일 종료
		if (in != NULL)
			fclose(in);

		//키입력을 선택하기위해 rand함수를 활용한다
		int randNum = rand() % (KM[keyCounter].left + KM[keyCounter].right + KM[keyCounter].jump);
		//값에따라 입력 선택
		if (randNum < KM[keyCounter].left)
		{
			keybd = LEFT;
		}
		else if (randNum < (KM[keyCounter].left + KM[keyCounter].jump))
		{
			keybd = JUMP;
		}
		else keybd = RIGHT;
				
		//위에서 선택된 값을 입력한다
		KM[keyCounter].input = keybd;
		keyInput(keybd);
		
		//점프의 중복 입력 방지를 위한 처리
		if (KM[keyCounter].input == JUMP)
			Sleep(100);

		//입력한 값에 대해 파일에 기록한다
		FILE* out = fopen(lay, "w");
		fprintf(out, "%d %d %d", KM[keyCounter].left, KM[keyCounter].right, KM[keyCounter].jump);
		fclose(out);

		//LEVEL개의 배열에 계속해서 데이터를 써준다
		keyCounter++;
		if (keyCounter == LEVEL)
		{
			keyCounter = 0;
		}
	}
}

int main()
{
	//openCL 사용
	ocl::setUseOpenCL(true);

	//콘솔창 위치 초기화
	HWND hWnd = GetConsoleWindow();
	SetWindowPos(hWnd, HWND_TOP, 280, 0, 0, 0, SWP_NOSIZE);


	//파일명을 위한 변수들
	char name0[] = "ms/";
	char bmp[] = "ms.bmp";
	char fname[100];

	//////////////////////////캡쳐를 위한 DIB정의
	// DIB의 형식을 정의한다.
	dib_define.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	dib_define.bmiHeader.biWidth = maxX - minX;
	dib_define.bmiHeader.biHeight = maxY - minY;
	dib_define.bmiHeader.biPlanes = 1;
	dib_define.bmiHeader.biBitCount = 24;
	dib_define.bmiHeader.biCompression = BI_RGB;
	dib_define.bmiHeader.biSizeImage = ((((maxX - minX) * 24 + 31) & ~31) >> 3) * (maxY - minY);
	dib_define.bmiHeader.biXPelsPerMeter = 0;
	dib_define.bmiHeader.biYPelsPerMeter = 0;
	dib_define.bmiHeader.biClrImportant = 0;
	dib_define.bmiHeader.biClrUsed = 0;

	// dib_define에 정의된 내용으로 DIB를 생성한다.
	HBITMAP h_bitmap = CreateDIBSection(h_screen_dc, &dib_define, DIB_RGB_COLORS, (void **)&p_image_data, 0, 0);

	// DIB 파일의 헤더 내용을 구성한다.
	ZeroMemory(&dib_format_layout, sizeof(BITMAPFILEHEADER));
	dib_format_layout.bfType = *(WORD*)"BM";
	dib_format_layout.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + dib_define.bmiHeader.biSizeImage;
	dib_format_layout.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	// 가상 DC에 이미지를 추출할 비트맵을 연결한다.
	SelectObject(h_memory_dc, h_bitmap);

	////////////////////////////////////////
	//매칭용 이미지들을 미리 grayscale
	cvtColor(num0, num0, CV_BGR2GRAY);
	cvtColor(gameover, gameover, CV_BGR2GRAY);
	cvtColor(spd, spd, CV_BGR2GRAY);
	cvtColor(spd9, spd9, CV_BGR2GRAY);

	cvtColor(tmp_sp, tmp_sp, CV_BGR2GRAY);
	cvtColor(tmp_char, tmp_char, CV_BGR2GRAY);

	cvtColor(trap11, trap11, CV_BGR2GRAY);
	cvtColor(trap12, trap12, CV_BGR2GRAY);
	cvtColor(trap12m, trap12m, CV_BGR2GRAY);
	cvtColor(trap13, trap13, CV_BGR2GRAY);
	cvtColor(trap14, trap14, CV_BGR2GRAY);
	cvtColor(trap15, trap15, CV_BGR2GRAY);

	cvtColor(trap21, trap21, CV_BGR2GRAY);
	cvtColor(trap22, trap22, CV_BGR2GRAY);
	cvtColor(trap23, trap23, CV_BGR2GRAY);


	//프로그램이 준비되면 아무키나 입력하고 시작한다
	cout << "Ready to start?\n";
	cin >> fout;
	
	cout << "5\n";
	Sleep(1000);
	cout << "4\n";
	Sleep(1000);
	cout << "3\n";
	Sleep(1000);
	cout << "2\n";
	Sleep(1000);
	cout << "1\n";
	Sleep(1000);
	cout << "start!!\n";

	//시간 측정용,  clock 함수를 이용해서 초기값을 설정한다
	start = clock();

	//기존의 플레이 정보를 load한다
	loadPlayCount();

	//시작하기전에 한번 리셋
	keybd_event(VK_F1, 0, 0, 0);
	Sleep(100);
	keybd_event(VK_F1, 0, KEYEVENTF_KEYUP, 0);

	//이미지 처리를 위한 파일명 설정
	sprintf(fname, "%s%d%s", name0, 0, bmp);
	
	//주요 프로그램 부분
	while (1)
	{
		//이 과정을 통해 시간값이 들어간 파일명을 만들어낸다
		// cap/ + 시간 + ms.bmp
		//sprintf(fname, "%s%d%s", name0, clock() - start, bmp);
		//cout << fname << "//";
		
		//출력을 위한 버퍼 초기화
		sprintf(fout, "");

		//위에서 만든 파일명과 capture함수로 화면을 캡쳐해서 저장한다
		capture(fname);

		//원본 이미지의 grayscale 작업을 한다
		ref_img = imread(fname);
		cvtColor(ref_img, ref_gray, CV_BGR2GRAY);


		//피드백을 위한 속도값 확인
		roi = setRoi(ref_gray, 190, 7, 200, 15); //스피드 위치
		checkSpd(roi, spd);
		roi = setRoi(ref_gray, 237, 6, 241, 14); //스피드 위치
		fullSpd(roi, spd9);


		//상황에 따른 게임 처리
		roi = setRoi(ref_gray, 126, 7, 150, 15);
		clearGame(roi, num0);
		roi = setRoi(ref_gray, 85, 53, 94, 63);
		gameOver(roi, gameover);


		//캐릭터 위치 확인
		roi = setRoi(ref_gray, 16, 156, 235, 195);
		charMatch(roi, tmp_char, res, ref_img, 0.73);

		//가장 위 함정 확인,최초엔 똑같아보이므로 멀티매칭으로 한번에 처리
		roi = setRoi(ref_gray, 95, 95, 142, 130);
		multiMatch(roi, trap11, res, ref_img, 1);

		//중앙 작은함정 확인
		roi = setRoi(ref_gray, 118, 118, 132, 131);
		simpleMatch(roi, trap12m, res, ref_img, 1, 1);

		roi = setRoi(ref_gray, 117, 133, 135, 139);
		simpleMatch(roi, trap13, res, ref_img, 1, 2);

		roi = setRoi(ref_gray, 108, 141, 143, 161);
		simpleMatch(roi, trap14, res, ref_img, 1, 3);

		roi = setRoi(ref_gray, 97, 167, 154, 188);
		simpleMatch(roi, trap15, res, ref_img, 1, 4);


		//좌우의 작은함정 확인
		roi = setRoi(ref_gray, 87, 126, 100, 139);
		simpleMatch(roi, trap12, res, ref_img, 1, 5);
		roi = setRoi(ref_gray, 139, 126, 150, 139);
		simpleMatch(roi, trap12, res, ref_img, 1, 6);

		roi = setRoi(ref_gray, 58, 142, 103, 160);
		simpleMatch(roi, trap14, res, ref_img, 1, 7);
		roi = setRoi(ref_gray, 149, 142, 192, 160);
		simpleMatch(roi, trap14, res, ref_img, 1, 8);


		roi = setRoi(ref_gray, 33, 167, 82, 188);
		simpleMatch(roi, trap15, res, ref_img, 1, 9);
		roi = setRoi(ref_gray, 160, 167, 218, 188);
		simpleMatch(roi, trap15, res, ref_img, 1, 0);


		//긴함정 확인
		roi = setRoi(ref_gray, 70, 133, 162, 140);
		longMatch(roi, trap21, res, ref_img, 1, 1);

		roi = setRoi(ref_gray, 55, 143, 180, 159);
		longMatch(roi, trap22, res, ref_img, 1, 2);

		roi = setRoi(ref_gray, 53, 157, 206, 190);
		longMatch(roi, trap23, res, ref_img, 1, 3);

		//결과 확인을 위한 이미지 저장
		//imwrite(fname, ref_img);


		//ai정보 갱신
		layer(atoll(fout));
	
		
		// 위의 함수들에서 만든 out의 정보를 출력
		//cout << fout << endl;

	}

	//캡쳐를 위해 사용했던 비트맵과 DC 를 해제한다.
	DeleteDC(h_memory_dc);
	if (NULL != h_bitmap) DeleteObject(h_bitmap);
	if (NULL != h_screen_dc) ReleaseDC(NULL, h_screen_dc);

	return 0;
}